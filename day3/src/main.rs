use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};
use counter::Counter;

fn column_from_file(filename: impl AsRef<Path>, column: usize) -> Vec<u32> {
    // Takes a column from file and returns it as a vector
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("No such line")
                   .chars()
                   .nth(column)
                   .expect("No such column")
                   .to_digit(2)
                   .expect("Char is not binary"))
        .collect()
}

// ---

fn main() {
    let mut all_columns: Vec<Vec<u32>> = (0..12).map(|i| column_from_file("./src/input.txt", i)).collect();
    // For each column group identical entries and sort according to most common first
    // let nums: Vec<Vec<(&u32, usize)>> = all_columns.iter()
    //                                                .map(|l| l.iter()
    //                                                          .collect::<Counter<_>>()
    //                                                          .most_common_ordered())
    //                                                .collect();
    // let most: Vec<String> = nums.iter().map(|x| x[0].0.to_string()).collect();
    // let least: Vec<String> = nums.iter().map(|x| x[1].0.to_string()).collect();
    // let gamma = isize::from_str_radix(&most.join(""), 2).unwrap();
    // let epsilon = isize::from_str_radix(&least.join(""), 2).unwrap();

    for k in 0..all_columns.len(){ 

        let mut last_one = false;
        for l in 0..all_columns[0].len(){
            if last_one == true && all_columns[0][l] != 2 {
                last_one = false;
                break;
            }
            if all_columns[0][l] != 2 && last_one == false {
                last_one = true;
            }
        }
        if last_one == true {
            break;
        }

        let most_common = all_columns[k].iter().collect::<Counter<_>>().most_common_ordered();
        let mut first_most:u32 = 2;
        let mut second_most:u32 = 2;
        for n in most_common {
            if *n.0 != 2 && first_most == 2{
                first_most = *n.0;
            }
            if *n.0 != 2 && first_most != 2 {
                second_most = *n.0;
            }
        }
        for i in 0..all_columns[k].len() {
            // if all_columns[0][i] == first_most {
            //     passed.push(i);
            // }
            if all_columns[k][i] != first_most {
                // all_columns[0].remove(i);
                for j in (0..all_columns.len()).rev() {
                    all_columns[j][i] = 2;
                }
            }
        }
    }

    let mut good_row:usize = 0;
    for k in 0..all_columns[0].len() {
        if all_columns[0][k] != 2 {
            good_row = k;
            break
        }
    }
    for val in all_columns {
        println!("{}", val[good_row])
    }
}