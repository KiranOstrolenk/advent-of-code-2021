use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

fn get_tuple(l: String)-> (String, i32) {
    let mut split_iter = l.split(" ");
            let direction = split_iter.next().expect("Can't find column").to_string();
            let number =  split_iter.next().expect("Can't find column").parse().expect("Not a number");
            (direction, number)
}

fn both(filename: impl AsRef<Path>) -> Vec<(String, i32)> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| get_tuple(l.expect("Could not parse line")))
        .collect()
}

// ---

fn main() {
    let directions = both("./src/input.txt");
    
    let mut hori: i32 = 0;
    let mut aim: i32 = 0;
    let mut depth: i32 = 0;

    for i in 0..directions.len() {
        if directions[i].0 == "forward" {
            hori += directions[i].1;
            depth += aim*directions[i].1;
        }
        else {
            if directions[i].0 == "up" {
                aim -= directions[i].1;
            }
            if directions[i].0 == "down" {
                aim += directions[i].1;
            }
        }
    }

    println!("hori: {}, aim: {}", hori, depth);
    println!("Answer: {}", hori*depth);


}