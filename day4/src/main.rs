use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<i32> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line").parse().expect("Line not int"))
        .collect()
}

fn calls(filename: impl AsRef<Path>) -> String {
    let file = File::open(filename).expect("no such file");
    let mut buf = BufReader::new(file);
    let mut first_line = String::new();
    buf.read_line(&mut first_line).expect("Unable to read first line");
    
    let len_withoutcrlf = first_line.trim_end().len();
    first_line.truncate(len_withoutcrlf);
    
    return first_line;
}

fn board_lines(filename: impl AsRef<Path>) -> Vec<Vec<&'static str>> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line").split(' ').collect())
        .collect()
}

// ---

fn main() {
    let first_line: String = calls("./src/calls.txt");
    let calls: Vec<&str> = first_line.split(',').collect();
    println!("{}", calls[99]);
    let board_lines = board_lines("./src/boards.txt");
    println!("{}", board_lines[0][0]);
}